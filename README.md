# Share on Confluence
Browse the web and quickly share links with your team **through Confluence**.

## Setup
1. Fork or clone this repository: `git clone git@bitbucket.org:compec/confluence-bookmarklet.git`.
2. Go right to `chrome://extensions`.
3. Click `Load unpacked extension...` and select `confluence-bookmarklet`.
4. To test your changes to the extension hit `cmd+R` or `ctrl+R` while on `chrome://extensions`.

## Features
* Hit `ctrl+shift+A` or `cmd+shift+A` while browsing to share the current webpage.
* Launch the Confluence sharer with **zero** lag time.
* Make your **link-sharing workflow** faster.
* Other teams around the world which relying Confluence can make great use of this extension.

## Under the hood
The Atlassian team built its regular, bookmarks-bar bookmarklet upon this simple URL (just as Facebook has its sharer.php):

* https://example.atlassian.net/wiki/plugins/sharelinksbookmarklet/bookmarklet.action
