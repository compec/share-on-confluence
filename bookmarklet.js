(function() {

	var atlassianComponent = "/wiki/plugins/sharelinksbookmarklet/bookmarklet.action?bookmarkedURL=";
	var organizationComponent = "";
	var logoComponent = "";
	var colorComponent = "";
	var iframeSrc = "";

	chrome.storage.sync.get({
		organizationComponent: "compec.atlassian.net",
		logoComponent: "http://compec.pe/images/nu_logocompec.svg",
		colorComponent: "0,0,0,0.75"
	}, function(items) {
		organizationComponent = items.organizationComponent;
		logoComponent = items.logoComponent;
		colorComponent = items.colorComponent;

		iframeSrc = "https://" + organizationComponent + atlassianComponent + encodeURIComponent(window.location.href);

		var bodyRef = document.getElementsByTagName("body")[0];
		var bodyHtml = bodyRef.innerHTML;

		if (window.location.href.indexOf("//github.com") != -1) {
			window.open(iframeSrc, '', 'width=700,height=600,personalbar=0,toolbar=0,scrollbars=1,resizable=0');
		} else {
			bodyRef.innerHTML = "<section id='share-on-confluence' style='z-index: 2147483647;left: 0;top: 0;width: 100%;height: 100%;margin:0;padding:0;position:fixed;text-align:center;background:rgba(" + colorComponent + ");'><p style='margin:1em 0;text-align:center;'><img style='height:65px;width:auto;text-align:center' height='65' width='auto' src='" + logoComponent + "'></p><iframe width='700' height='530' style='border:0;margin-bottom:1em;' src='" + iframeSrc + "'></iframe><p style='margin:0;'><button id='close-share-on-confluence'>Close</button></p></section>" + bodyHtml; // Add, very rudimentarily, the iframe portion
		}

		var overlayRef = document.getElementById("share-on-confluence");
		overlayRef.onclick = function() {
			bodyRef.removeChild(overlayRef);
		}
	});

})();
