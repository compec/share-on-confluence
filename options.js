(function(){

function save_options() {
	var organizationURL = document.getElementById('organization').value;
	var logoURL = document.getElementById('logo').value;
	var colorRGBA = document.getElementById('color').value;
	
	chrome.storage.sync.set({
		organizationComponent: organizationURL,
		logoComponent: logoURL,
		colorComponent: colorRGBA
	}, function() {
		// Update status to let user know options were saved.
		var status = document.getElementById('save');
		status.textContent = 'Saved.';
		setTimeout(function() {
			status.textContent = 'Save';
		}, 1000);
	});
}

function restore_options() {
	chrome.storage.sync.get( null, function(items) {
		document.getElementById('organization').value = items.organizationComponent;
		document.getElementById('logo').value = items.logoComponent;
		document.getElementById('color').value = items.colorComponent;
	});
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('organization').addEventListener('keypress', save_options);
document.getElementById('save').addEventListener('click', save_options);

})();